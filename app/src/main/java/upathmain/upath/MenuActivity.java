package upathmain.upath;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_menu);
    }

    public void newPath(android.view.View view) {
        Intent tempIntent = new Intent(this, CoreActivity.class);
        this.startActivity(tempIntent);
    }

    public void profile(android.view.View view) {
        Intent tempIntent = new Intent(this, ProfileActivity.class);
        this.startActivity(tempIntent);
    }

    public void history(android.view.View view) {
        Intent tempIntent = new Intent(this, HistoryActivity.class);
        this.startActivity(tempIntent);
    }

    public void settings(android.view.View view) {
        Intent tempIntent = new Intent(this, SettingsActivity.class);
        this.startActivity(tempIntent);
    }

    public void weather(android.view.View view) {
        Intent tempIntent = new Intent(this, WeatherActivity.class);
        this.startActivity(tempIntent);
    }

}
