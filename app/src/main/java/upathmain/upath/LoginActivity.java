package upathmain.upath;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);
    }

    public void login(View tempView) {
        EditText emailEditText = findViewById(R.id.email);
        EditText passwordEditText = findViewById(R.id.password);

        String username = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (!validate()) {
            onLoginFailed();
            return;
        }

//        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
//                R.style.Theme_AppCompat_DayNight_Dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();
        onLoginSuccess();
        /*new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
//                        progressDialog.dismiss();
                    }
                }, 3000);*/


    }

    void onLoginSuccess() {
        String[] params = new String[4];

        EditText emailEditText = findViewById(R.id.email);
        EditText passwordEditText = findViewById(R.id.password);

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        params[0] = "email";
        params[1] = email;
        params[2] = "password";
        params[3] = password;

        ServerRequestLogin serverLogin = new ServerRequestLogin();
        try
        {
            String response = serverLogin.execute(params).get();
            JSONObject json = null;
            try {
                //Tranform the string into a json object
                json = new JSONObject(response);
                String type = json.getString("type");
                String message = json.getString("message");
                Log.e("ceva", type);
                if(type.equals("success"))
                {
                    Intent tempIntent = new Intent(this, MenuActivity.class);
                    this.startActivity(tempIntent);
                }
                else
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        catch(InterruptedException e)
        {

        }
        catch(ExecutionException ase)
        {

        }

    }

    void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
    }

    public boolean validate() {
        boolean valid = true;

        EditText emailEditText = findViewById(R.id.email);
        EditText passwordEditText = findViewById(R.id.password);
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError("Enter a valid email address");
            valid = false;
        } else {
            emailEditText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordEditText.setError("Password must be between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordEditText.setError(null);
        }

        return valid;
    }

    public void createAccount(View tempView) {
        Intent tempIntent = new Intent(this, SignUpActivity.class);
        this.startActivity(tempIntent);
    }
}
