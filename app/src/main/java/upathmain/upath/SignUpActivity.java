package upathmain.upath;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class SignUpActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_sign_up);
    }

    public void signUp(View tempView) {
        EditText emailEditText = findViewById(R.id.email);
        EditText usernameEditText = findViewById(R.id.username);
        EditText passwordEditText = findViewById(R.id.password);

        String email = emailEditText.getText().toString();
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (!validate()) {
            signUpFailed();
            return;
        }

        String[] params = new String[8];

        params[0] = "email";
        params[1] = email;
        params[2] = "password";
        params[3] = password;
        params[4] = "password_confirmation";
        params[5] = password;
        params[6] = "name";
        params[7] = username;

        ServerRequestSignUp serverLogin = new ServerRequestSignUp();
        try
        {
            String response = serverLogin.execute(params).get();
            JSONObject json = null;
            try {
                //Tranform the string into a json object
                json = new JSONObject(response);
                String type = json.getString("type");
                String message = json.getString("message");
                Log.e("ceva", type);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                if(type.equals("success"))
                {
                    Intent tempIntent = new Intent(this, LoginActivity.class);
                    this.startActivity(tempIntent);
                }

            } catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }
        }
        catch(InterruptedException e)
        {

        }
        catch(ExecutionException ase)
        {

        }


    }

    public boolean validate() {
        boolean valid = true;

        EditText emailEditText = findViewById(R.id.email);
        EditText passwordEditText = findViewById(R.id.password);
        EditText confirmPasswordEditText = findViewById(R.id.passwordConfirmation);

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError("Enter a valid email address");
            valid = false;
        } else {
            emailEditText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordEditText.setError("Passwords must be between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passwordEditText.setError(null);
        }

        if (!password.equals(confirmPassword)) {
            confirmPasswordEditText.setError("Passwords must be identical");
        }

        return valid;
    }

    public void signUpFailed() {
        Toast.makeText(getBaseContext(), "Sign-up failed", Toast.LENGTH_LONG).show();
    }
}
