package upathmain.upath;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mihai on 25.11.2017.
 */

public class ServerRequestSignUp extends AsyncTask<String[], Void, String> {
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    @Override
    protected String doInBackground(String[]... string) {
        //try {
        String ceva = "";
        try {
            String site = "http://10.42.0.1/register";
            String current_action = "/register"; // ?

            HashMap<String, String> hash = new HashMap<String, String>();
            String[] urls = string[0];

            for(int i = 0; i < urls.length; i += 2)
            {
                String a = urls[i];
                String b = urls[i + 1];
                hash.put(a, b);
                Log.e("rasp", a + " " + b);
            }
            Log.e("rasp", site);
            URL obj = new URL(site);
            try {
                Log.e("rasp", obj.toString());
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                //con.setRequestProperty("User-Agent", USER_AGENT);
                // For POST only - START
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                os.write(getPostDataString(hash).getBytes());
                os.flush();
                os.close();
                // For POST only - END
                int responseCode = con.getResponseCode();
                Log.e("rasp", "response code-ul e " + Integer.toString(responseCode));
                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    // print result
                    Log.e("raspunsul", response.toString());
                    ceva = response.toString();
                }
                else
                {
                    Log.e("rasp", "POST request not worked");
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        catch (MalformedURLException e)
        {
            Log.e("naspa", "E corupt!");
        }
        return ceva;
    }
}

